from random import randint

# Generates a list of four, unique, single-digit numbers
def generate_secret():
    # TODO:
    random_num = randint(1000,9999)
    return random_num
    # You need to delete the word "pass" and write your
    # function, here.


def convert_guess(param):
    converted_num = []
    param= int(param)
    converted_num.append(param)
    return converted_num

def count_the_matches(secret, guess):
    count=0
    secret = str(secret)
    guess = str(guess)
    pairs = zip(secret,guess)
    for x,y in pairs:
         if x==y:
            count+=1
    return count

def count_common_entries(guess,secret):
    entries =0
    secret = str(secret)
    guess = str(guess)
    for i in secret:
        for q in guess:
            if i == q:
                entries+=1
    return entries  


def play_game():
    print("Let's play Bulls and Cows!")
    print("--------------------------")
    print("I have a secret four numbers.")
    print("Can you guess it?")
    print("You have 20 tries.")
    print()

    secret = generate_secret()
    number_of_guesses = 20

    for i in range(number_of_guesses):
        prompt = "Type in guess #" + str(i + 1) + ": "
        guess = input(prompt)

        # TODO:
        # while the length of their response is not equal
        while len(guess)>4 and len(guess)<4:
            print("You must enter 4 digit number.")
            guess = input(prompt)
        # to 4,
        #   tell them they must enter a four-digit number
        #   prompt them for the input, again




        # TODO:
        # if the number of exact matches is four, then
        matches = count_the_matches(secret, guess)
        if matches == 4:
            print("The player has correctly guessed. ")
        # the player has correctly guessed! tell them
        # so and return from the function.

        # TODO:
        # report the number of "bulls" (exact matches)
        bulls = matches
        print(bulls)
        # TODO:
        commons = count_common_entries(secret,guess)
        
        cows = commons - matches
        print(cows)
        # report the number of "cows" (common entries - exact matches)

    # TODO:
    # If they don't guess it in 20 tries, tell
    print("Sorry, boo! The secret was : "+ secret)
    # them what the secret was.


# Runs the game
def run():
    # TODO: Delete the word pass, below, and have this function:
    # Call the play_game function
    play_game()

    # Ask if they want to play again
    # While the person wants to play the game
    #   Call the play_game function
    #   Ask if they want to play again


if __name__ == "__main__":
    run()
